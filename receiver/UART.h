/*
 * UART.h
 *
 *  Created on: Aug 9, 2020
 *      Author: lenovo
 */

#ifndef UART_H_
#define UART_H_

void Uart_init(long BaudRate);
void UART_send_byte( char byte);
void UART_send_string( char *ptr);
char UART_recieve_byte(void);
void UART_recieve_string(char * ptr);
void UART_SendArray(unsigned char *array, int Length);
int search ();
char ReadUntilExpectedKeyword (char * ExpectedKeyword, char
ExpectedKeyword_Length);
void itoa_Convert (int Number , char * NumberASCII_ptr ,char*
NumberOFDigits_ptr);

#endif /* UART_H_ */
