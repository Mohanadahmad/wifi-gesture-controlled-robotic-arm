#include"avr/io.h"
#include<util/delay.h>
#include "UART.h"
#include<avr/interrupt.h>
#include <stdlib.h>
volatile int counter1 =0 ,counter2 =0 ;
char arr1[4],arr2[4],arr3[4],arr4[4],arr5[4];
int a,b,c,d,e,search_result=0;

#define OK 1
#define FAIL 2
#define ERROR 3
#define READY 4

void pwm_init()

/* initalize all the timers and enable all interrupts and set the pwm pins as output*/
{
TCCR1A |= (1<<COM1A1) | (1<<COM1B1) | (1<<WGM11);
TCCR1B |= (1<<WGM12) | (1<<WGM13) | (1<<CS11) |
(1<<CS10);
ICR1 = 4999;
TCCR0A |=(1<<COM0A1)|(1<<COM0A0)|(1<<COM0B1)|(1<<COM0B0)|(1<<WGM01)|(1<<WGM00);
TCCR0B |=(1<<CS02);
TCCR2A |=(1<<COM2A1)|(1<<COM2A0)|(1<<COM2B1)|(1<<COM2B0)|(1<<WGM21)|(1<<WGM20);
TCCR2B |=(1<<CS22)|(1<<CS21);
TIMSK0=(1<<TOIE0);
TIMSK2=(1<<TOIE2);
ASSR|= (1<<EXCLK);
DDRD= (1<<3)|(1<<6)|(1<<5) ;
DDRB=(1<< 3)|(1<<2)|(1<<1);
}

void main(void)
{
pwm_init();
sei(); // enable global interrupts 
int index,length,i,j;

Uart_init(9600); // initalize UART baudrate
UART_send_string("AT+RST\r\n"); // send reset command to wifi module
while(search_result!=READY)
{
search_result= search();
}
search_result=0;
UART_send_string("AT+CWQAP\r\n"); // send command to make wifi module quit any accesspoint
while(search_result!=OK)
{
search_result= search();
}
search_result=0;
UART_send_string("ATE1\r\n"); // enable echo response
while(search_result!=OK)
{
search_result= search();
}
search_result=0;
while(search_result!=OK)
{
search_result= search();
}
search_result=0;
UART_send_string("AT+CIPMUX=1\r\n"); // single mode
while(search_result!=OK)
{
search_result= search();
}
search_result=0;
UART_send_string("AT+CIPSERVER=1,222\r\n"); // create server port number 222
while(search_result!=OK)
{
search_result= search();
}
search_result=0;
 UART_send_string("AT+CIFSR\r\n"); // get local ip address
while(search_result!=OK)
{
search_result= search();
}
search_result=0;
UART_send_string("AT+CWSAP=\"modulereciever\",\"12345678\",4,3\r\n"); // set accesspoint name and password
while(search_result!=OK)
{
search_result= search();
}
search_result=0;
while (1)
{char rxdata[30]={0};
index=0;
j=0;
for (i=0;i<=29;i++)
{
rxdata[i]=UART_recieve_byte(); // receive bytes continously until a newline occurs
if (rxdata[i]=='\n'){length=i-1; break ;}
}
for (i=0;i<length;i++)
{
if (rxdata[i]==':'){ index = i ; break ;} // each 
}
char arr[length-index-1];
for ( i=index+1;i<length;i++)
{
arr[j]=rxdata[i];
j++;
}
for (i=0;i<length;i++)
{
if (arr[i]=='A'){arr[i]='\0';}
}
for (i=0;i<=2;i++)
{arr1[i]=arr[i];
arr2[i]=arr[i+3];
arr3[i]=arr[i+6];
arr4[i]=arr[i+9];
arr5[i]=arr[i+12];}
arr1[3]='\0';
arr2[3]='\0';
arr3[3]='\0';
arr4[3]='\0';
arr4[3]='\0';
a= atoi (arr1);
b= atoi (arr2);
OCR1A=a;
OCR1B=b;
c= atoi (arr3);
d=atoi (arr4);
e=atoi (arr5);
}
}
ISR(TIMER0_OVF_vect)
{
counter1 ++ ;
if (counter1 ==5)
{
OCR0A=c;
OCR0B=d;
counter1 =0 ;
}
else {OCR0A=255;
OCR0B=255 ;
}
}
ISR(TIMER2_OVF_vect)
{
counter2 ++ ;
if (counter2 ==5)
{
OCR2A=e;
counter2 =0 ;
}
else {OCR2A=255;
}
}

