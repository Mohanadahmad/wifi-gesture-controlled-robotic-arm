#include "UART.h"
#include "avr/io.h"
void Uart_init(long BaudRate)
{
UCSR0B=(1<<RXEN0)|(1<<TXEN0);
UCSR0C=(1<<UCSZ01)|(1<<UCSZ00);
UBRR0L=(unsigned char)(((F_CPU)/(16*BaudRate))-1);
}
void UART_send_byte( char byte)
{
while(!(UCSR0A &(1<<UDRE0))); // while shift register flag != 1(not empty))
UDR0=byte;
}
void UART_send_string( char *ptr)
{
int i=0;
while(ptr[i] != '\0')
{
UART_send_byte(ptr[i]);
i++;
}
UART_send_byte(ptr[i]);
}
char UART_recieve_byte(void)
{
while((UCSR0A & (1<<RXC0)) ==0);
return UDR0 ;
}
void UART_recieve_string(char * ptr)
{
int i=0;
ptr[i]=UART_recieve_byte();
while(ptr[i] !='\0')
{
i++;
ptr[i]=UART_recieve_byte();
}
ptr[i]='\0';
}
void UART_SendArray(unsigned char *array, int Length)
{
unsigned char ArrayIndex;
for(ArrayIndex = 0; ArrayIndex< Length; ArrayIndex++)
{
UART_send_byte(array[ArrayIndex]);
}
}
int search (){ int x=0 ;
char rxdata[30]={0};
int i ;
for (i=0;i<=29;i++)
{
rxdata[i]=UART_recieve_byte();
if (rxdata[i]=='\n') break ;
//UART_send_byte(rxdata[i]);
}
for ( i=0;i<=30 ;i++)
{if ((rxdata[i]=='O')&&(rxdata[i+1]=='K')) { x=1 ; break ;}
else if ((rxdata[i]=='F') &&( rxdata[i+1]=='A') &&( rxdata[i+2]=='I') &&
(rxdata[i+3]=='L') )
{ x=2 ; break;}
else if ((rxdata[i]=='E') &&( rxdata[i+1]=='R') &&( rxdata[i+2]=='R') &&
(rxdata[i+3]=='O')&&(rxdata[i+4]=='R') )
{ x=3 ; break;}
else if ((rxdata[i]=='r') &&( rxdata[i+1]=='e') &&( rxdata[i+2]=='a') &&
(rxdata[i+3]=='d')&&(rxdata[i+4]=='y') )
{ x=4 ; break;}
}
return x;}
