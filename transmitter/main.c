#include "avr/io.h"
#include <util/delay.h>
#include "UART.h"
#include"ADC.h"
#include <stdlib.h>
#include <string.h>

#define OK 1
#define FAIL 2
#define ERROR 3
#define READY 4

/* function used to map the ADC readings to a Servo motor angle*/
float map (float input)
{ //output = output_start + ((output_end - output_start) / (input_end -input_start)) * (input - input_start)
float output = 0.0 + ( ( 180.0 - 0.0) / ( (1.2) - (0.5) ) ) * (input -
(0.5));
return output ;
}


unsigned int pwm_Set(unsigned char id ,float Angle)
{
unsigned int x ;
float time ;
time =(1.92/180.0)*Angle + (0.5) ; // convert the angle to the time needed to achieve width of pulse
if(id==1){
time = (time/0.004);} // for timer 1 convert the time to no of pulses
else{time=(time/0.016);}// for timer 2 convert the time to no of pulses
x= (unsigned int )time ;
return x;
}

void main()
{
ADC_init (); // initalize ADC
unsigned int search_result=0,i ,a,b,c,d,e; 
char arr1[3],arr2[3],arr3[3],arr4[3],arr5[3],arr6[18];
float volt ;
Uart_init(9600); // initalize UART BAUDRATE
UART_send_string("AT+RST\r\n"); // send reset command to ESP8266 module
while (search_result!=READY) // wait for ready response
{search_result=search();}
search_result=0;
UART_send_string("AT+CWQAP\r\n"); // disconnect esp8266 module from any currently connected accesspoint
while (search_result!=OK)  // wait for ok response
{search_result=search();}
search_result=0; 
UART_send_string("AT+CWMODE=1\r\n"); //set esp8266 module to client mode
while (t!=OK) // wait for ok response
{search_result=search();}
search_result=0;
UART_send_string("AT+CWJAP=\"modulereciever\",\"12345678\"\r\n"); // join accesspoint name and password
while (search_result!=OK) // wait for ok
{
search_result= search () ;
if (search_result==FAIL){
UART_send_string("AT+CWJAP=\"modulereciever\",\"12345678\"\r\n"); // if failed keep trying 
}
}
search_result=0;
DDRB=(1<<3); // lighten two pins to show that connections are successful
PORTB= (1<<3);
search_result=0;
UART_send_string("AT+CIPMUX=1\r\n");  // make esp8266 module single connection
while (search_result!=OK)
{search_result=search();}
search_result=0;
UART_send_string("AT+CIPSTART=2,\"TCP\",\"192.168.4.1\",222\r\n"); //start transfer of data using tcp ip module
while (search_result!=OK)
{
search_result= search () ;
if (search_result==ERROR){
UART_send_string("AT+CIPSTART=2,\"TCP\",\"192.168.4.1\",222\r\n");
}
}
search_result=0;
DDRB=(1<<4);
PORTB= (1<<4); lighten pin to show transfer started
search_result=0;
while (1){
volt= (ADC_read('0') * 5)/1024 ; // read from ADCchannel 0
volt= map(volt); // convert volt to angle
a=pwm_Set(1,v);  // calculate the no of pulses for this angle
itoa(a,arr1,10); // convert the number to ASCII in order to be transfered by UART
volt= (ADC_read('1')*5)/1024; // read from ADC channel 1
volt= map(volt); // convert volt to angle
b=pwm_Set(1,volt);  // calculate the no of pulses for this angle
itoa (b, arr2,10); // convert the number to ASCII in order to be transfered by UART
volt= (ADC_read('2')*5)/1024; // read from ADC channel 2
volt= map(volt);  // convert volt to angle
c=255-pwm_Set(0,volt); // calculate the no of pulses for this angle
itoa (c, arr3,10);  // convert the number to ASCII in order to be transfered by UART
volt= (ADC_read('3')*5)/1024;  // read ADC channel 3
volt= map(volt); // convert volt to angle
d=255-pwm_Set(0,volt); //calculate no of pulses for this angle
itoa (d, arr4,10); // convert number to Ascii in order to be transferred by UART
volt (ADC_read('4')*5)/1024; // read ADC channel 4
volt= map(volt); // convert volt to angle
e=255-pwm_Set(0,v); // calculate no of pulses 
itoa (e, arr5,10); // convert number to ASCII to transfer by UART

/* copy all the arrays in one array to be transferred as a set of data*/
for (i=0 ;i<=2;i++)
{arr6[i]=arr1[i];
arr6[i+3]=arr2[i];
arr6[i+6]=arr3[i];
arr6[i+9]=arr4[i];
arr6[i+12]=arr5[i];}
for (i=0;i<=14;i++)
{if (arr6[i]=='\0') arr6[i]='A';}
arr6[15]='\r';
arr6[16]='\n';
arr6[17]='\0';
UART_send_string("AT+CIPSEND=2,17\r\n"); // start sending
while (search_result!=OK||search_result==FAIL)
{search_result=search();}
search_result=0;
UART_send_string(arr6); // send the data set

}}
