/*
 * ADC.h
 *
 *  Created on: Aug 9, 2020
 *      Author: lenovo
 */

#ifndef ADC_H_
#define ADC_H_

void ADC_init(void);
int ADC_read(unsigned char channel);

#endif /* ADC_H_ */
