#include <avr/io.h>
void ADC_init(void)
{
ADMUX=(1<<REFS0)|(1<<MUX0);
ADCSRA=(1<<ADEN)|(1<<ADPS1)|(1<<ADPS2);
}
int ADC_read(unsigned char channel)
{
if (channel=='0'){ADMUX=0x40;}
else if (channel=='1'){ADMUX=0x41;}
else if (channel=='2'){ADMUX=0x42;}
else if ( channel=='3') { ADMUX=0x43;}
else if (channel=='4') {ADMUX=0x44;}
ADCSRA |=(1<<ADSC);
while(!(ADCSRA & (1 << ADIF)));
ADCSRA |= (1 << ADIF);
return ADC;
}
