#include "UART.h"
#include<avr/io.h>
#include <stdlib.h>
#include <string.h>

#define OK 1
#define FAIL 2
#define ERROR 3
#define READY 4
void Uart_init(long BaudRate)
{
UCSRB=(1<<RXEN)|(1<<TXEN);
UCSRC=(1<<URSEL)|(1<<UCSZ0)|(1<<UCSZ1);
UBRRL=(unsigned char)(((F_CPU)/(16*BaudRate))-1);
}
void UART_send_byte( char byte)
{
while(!(UCSRA &(1<<UDRE))); // while shift register flag != 1 (notempty))
UDR=byte;
}
void UART_send_string( char *ptr)
{
int i=0;
while(ptr[i] != '\0')
{
UART_send_byte(ptr[i]);
i++;
}
UART_send_byte(ptr[i]);
}
char UART_recieve_byte(void)
{
while((UCSRA & (1<<RXC)) ==0);
return UDR ;
}
void UART_recieve_string(char * ptr)
{
int i=0;
ptr[i]=UART_recieve_byte();
while(ptr[i] !='\0')
{
i++;
ptr[i]=UART_recieve_byte();
}
ptr[i]='\0';
}
void UART_SendArray(unsigned char *array, int Length)
{
unsigned char ArrayIndex;
for(ArrayIndex = 0; ArrayIndex< Length; ArrayIndex++)
{
UART_send_byte(array[ArrayIndex]);
}
}
int search (){ int search_result=0 ;
char rxdata[30]={0};
int i ;
for (i=0;i<=29;i++)
{
rxdata[i]=UART_recieve_byte();
if (rxdata[i]=='\n') break ;
//UART_send_byte(rxdata[i]);
}
for ( i=0;i<=30 ;i++)
{if ((rxdata[i]=='O')&&(rxdata[i+1]=='K')) { search_result=OK ; break ;}
else if ((rxdata[i]=='F') &&( rxdata[i+1]=='A') &&( rxdata[i+2]=='I') &&
(rxdata[i+3]=='L') )
{ search_result=FAIL ; break;}
else if ((rxdata[i]=='E') &&( rxdata[i+1]=='R') &&( rxdata[i+2]=='R') &&
(rxdata[i+3]=='O')&&(rxdata[i+4]=='R') )
{ search_result=ERROR ; break;}
else if ((rxdata[i]=='r') &&( rxdata[i+1]=='e') &&( rxdata[i+2]=='a') &&
(rxdata[i+3]=='d')&&(rxdata[i+4]=='y') )
{ search_result=READY ; break;}
}
return search_result;}
char ReadUntilExpectedKeyword (char * ExpectedKeyword, char
ExpectedKeyword_Length)
{int c ;
char index = 0 ;
char RxData[20] = {0}; /* array to receive uart in it and compare with
Expected Array */
for (index =0 ; index < ExpectedKeyword_Length; index++)
{
RxData[index]= UART_recieve_byte() ;
}
while (1)
{
for (index =0 ; index < ExpectedKeyword_Length; index++)
{
if (RxData[index] != ExpectedKeyword[index])
{
break;
}
if (index == ExpectedKeyword_Length -1)
{
return 1;
}
}
/* shift one byte and receive new byte then compare */
for (index =0 ; index < ExpectedKeyword_Length-1; index++)
{
RxData[index] = RxData[index+1];
}
RxData[ExpectedKeyword_Length-1]=UART_recieve_byte();
}
return 0;
}
void itoa_Convert (int Number , char * NumberASCII_ptr ,char*
NumberOFDigits_ptr)
{
unsigned char ArrayIndex = 0;
unsigned char NumberOfDigits = 0;
int Number_temp = Number;
while ( Number == 0 )
{
NumberASCII_ptr[ArrayIndex] = '0';
return;
}
/* get Number of digits of the wanted number to be converted */
while ( Number_temp > 0 )
{
Number_temp = Number_temp/10;
NumberOfDigits++;
}
*NumberOFDigits_ptr = NumberOfDigits;
for ( ArrayIndex =0 ; ArrayIndex < NumberOfDigits ;
ArrayIndex++)
{
NumberASCII_ptr[ArrayIndex] = Number %10 +'0';
Number = Number/10;
}
/* rotate , using Number as a temp */
for ( ArrayIndex =0 ; ArrayIndex < NumberOfDigits/2 ;
ArrayIndex++)
{
Number = NumberASCII_ptr[ArrayIndex];
NumberASCII_ptr[ArrayIndex] =
NumberASCII_ptr[NumberOfDigits-ArrayIndex-1];
NumberASCII_ptr[NumberOfDigits-ArrayIndex-1] = Number;
}
}
